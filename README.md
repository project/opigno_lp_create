## CONTENTS OF THIS FILE

- Introduction
- Features
- Requirements
- Installation
- Configuration
- Maintainers

## INTRODUCTION

The Opigno Learning Path Creation module is a versatile tool that enhances the capabilities of the Opigno learning platform. It seamlessly integrates various activities, modules, and groups, enabling the creation of engaging and comprehensive learning paths. This module simplifies the management of Opigno resources through a user-friendly interface, offering effortless control over activities, modules, and groups.

- For a full description of the module visit:
  https://www.drupal.org/project/opigno_lp_create
    
- To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/opigno_lp_create

## FEATURES

- Diverse Content Integration: The Opigno Learning Path Creation module incorporates various types
of content, including quiz questions, multiple-choice options, and interactive drag-and-drop exercises. This diversity allows for the creation of engaging and interactive learning experiences.

- Seamless Navigation: The elements within this module work harmoniously to establish a smooth and integrated navigation system within the Opigno platform. This seamless navigation enhances the user experience.

- Streamlined Interface: A simple and intuitive form consolidates all Opigno features into a unified and user-friendly interface. This simplifies the management of activities, modules, and groups, making it accessible to users of all levels.

- Effortless Control: Users can effortlessly control Opigno activities, modules, and groups through this single interface. This streamlines the management process, reducing complexity and saving time.

- Complete Learning Path Creation: Users can create comprehensive learning paths from a single page, eliminating the need to navigate through multiple pages or interfaces. This efficiency promotes a more efficient workflow.

- Simplified Management: The module simplifies the process of both creating and managing learning paths. It streamlines administrative tasks, allowing educators and administrators to focus on content and delivery.

## REQUIREMENTS

Ensure that you have Opigno and its dependencies installed and configured before using this module. Refer to Opigno's documentation for further information on integrating this module with your Opigno-based eLearning platform.

## INSTALLATION

- Install the Opigno Learning Path Creation module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.
- Enable the module via the Drupal administration interface or by using Drush:
    "drush en opigno_learning_path_manager -y"

## CONFIGURATION

1. After successfully installing the module Opigno Learning Path Creation, users are guided to the 
  "/admin/opigno_lms/lms-config" URL to configure default settings. 
  This installation setup minimizes the need for manual configuration, ensuring a smooth onboarding process.

2. Users can access the Opigno Learning Path Creation page conveniently using the 
  "/admin/opigno_lms/custom_form" URL. 
  This accessibility encourages educators to create effective learning paths with ease.

## MAINTAINERS

- Dharmendra Saini (dharmendra-virasat) - https://www.drupal.org/u/dharmendra-virasat

# Supporting organization

- Virasat Solutions - https://www.drupal.org/virasat-solutions

Expert in Drupal design, development, Theming, and migration, as well as UI/UX design which makes us a one-stop Drupal development company.
