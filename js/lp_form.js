(function ($, Drupal) {
    Drupal.behaviors.learningPathForm = {
        attach: function (context, settings) {
            $('#module-fields-div-activity').hide();
            $('.module-list-field').change(
                function () {
                    console.log($(this).val())
                    
                    console.log('llllll')
                    if ($('.checkbox-for-module-fields').is(':checked')) {
                        console.log($('.module-list-field').val())
                        if ($('.module-list-field').val() != '_none') {
                            $('.module-name-field').val('')
                            $('.module-description-field').val('')
                            $("#module-fields-div").hide();
                            $('.checkbox-for-module-fields').prop('checked', false);
                        }
                    } else {
                        console.log($('.module-name-field').val())
                        $('.module-name-field').val('')
                        $('.module-description-field').val('')
                        $("#module-fields-div").hide();
                    } 
                }
            );

            $(document).on("click", ".checkbox-for-module-fields", function (e) {
                    if($(this).is(':checked')) {

                        $("#module-fields-div").show();
                        $("#module-list-div").hide();
                        $('.module-list-field').val('_none')
                        $('#module-fields-div-activity').show();
                        
                    }else{
                        $('.module-name-field').val('')
                        $('.module-description-field').val('')
                        $("#module-list-div").show();
                        $('#module-fields-div-activity').hide();
                       
                    }
                }
            );

            $(function () {
                    if ($('.checkbox-for-module-fields').is(':checked')) {
                        $("#module-list-div").hide();
                    }
                }
            );        
        } 
    };
})(jQuery, Drupal);