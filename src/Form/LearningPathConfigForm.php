<?php

namespace Drupal\opigno_lp_create\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class restitutes the learning path config form.
 */
class LearningPathConfigForm extends ConfigFormBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'opigno_lp_create.lms_config';

  /**
   * {@inheritdoc}
   */
  private $database;

  /**
   * Constructs.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database service.
   */
  public function __construct(Connection $connection) {
    $this->database = $connection;
  }

  /**
   * Create container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lms_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Builds the configuration form for learning path settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array that defines the configuration form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    // Get certificate list.
    $query = $this->database->select('opigno_certificate', 'oc');
    $query->fields('oc', ['id', 'bundle', 'uuid', 'label', 'uid']);
    $certificate_results = $query->execute()->fetchAll();
    // Create options for certificate.
    $create_cer_options = ['_none' => '- None -'];
    if (!empty($certificate_results)) {
      foreach ($certificate_results as $value) {
        if (!empty($value)) {
          $create_cer_options[$value->bundle] = [$value->id => $value->label];
        }
      }
    }

    $form['certificate'] = [
      '#type' => 'select',
      '#title' => $this->t('Certificate'),
      '#required' => TRUE,
      '#default_value' => $config->get('certificate'),
      '#options' => $create_cer_options,

    ];
    $form['minimum_score'] = [
      '#type' => 'textfield',
      '#attributes' => [
    // Insert space before attribute name :)
        'type' => 'number',
      ],
      '#title' => $this->t('Maximum score'),
      '#default_value' => $config->get('minimum_score'),
    ];
    $form['pass_percentage'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'type' => 'number',
      ],
      '#title' => $this->t('Pass Percentage score'),
      '#default_value' => $config->get('pass_percentage'),
    ];
    $form['takes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of attempts'),
      '#default_value' => $config->get('takes'),
    ];

    $form['hide_results'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Review correct answers'),
      '#default_value' => $config->get('hide_results'),
    ];
    $form['hide_check_answers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide check answers'),
      '#default_value' => $config->get('hide_check_answers'),
    ];
    $form['hide_result_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide result page'),
      '#default_value' => $config->get('hide_result_page'),
    ];
    $form['hide_solution_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide solution button'),
      '#default_value' => $config->get('hide_solution_button'),
    ];
    $form['hide_retry_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide retry button'),
      '#default_value' => $config->get('hide_retry_button'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * Save datas for learning path settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(static::SETTINGS)
      ->set('certificate', $form_state->getValue('certificate'))
      ->set('minimum_score', $form_state->getValue('minimum_score'))
      ->set('takes', $form_state->getValue('takes'))
      ->set('hide_results', $form_state->getValue('hide_results'))
      ->set('hide_check_answers', $form_state->getValue('hide_check_answers'))
      ->set('hide_result_page', $form_state->getValue('hide_result_page'))
      ->set('hide_solution_button', $form_state->getValue('hide_solution_button'))
      ->set('hide_retry_button', $form_state->getValue('hide_retry_button'))
      ->set('pass_percentage', $form_state->getValue('pass_percentage'))
      ->save();
  }

}
