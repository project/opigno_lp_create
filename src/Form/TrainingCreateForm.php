<?php

namespace Drupal\opigno_lp_create\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\opigno_lp_create\OpignoLPManager;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * This class restitutes the training create form.
 */
class TrainingCreateForm extends FormBase {

  /**
   * Include the OpignoLPManager service.
   *
   * @var \Drupal\opigno_lp_create\OpignoLPManager
   */
  protected $opignoLpCreate;

  /**
   * Include the Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  protected $renderer;

  /**
   * Constructs.
   *
   * @param \Drupal\opigno_lp_create\OpignoLPManager $opignoLpCreate
   *   provides the OpignoLPManager service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(OpignoLPManager $opignoLpCreate, Connection $connection, RequestStack $request_stack, MessengerInterface $messenger, ConfigFactoryInterface $configFactory, RendererInterface $renderer) {
    $this->opignoLpCreate = $opignoLpCreate;
    $this->database = $connection;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
    $this->renderer = $renderer;
  }

  /**
   * Create container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('opigno_lp_create.lp_create'),
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('renderer')

    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'training_create_form';
  }

  /**
   * Builds the form for training.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array that defines the formbase form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('opigno_lp_create.lms_config');

    $request = $this->requestStack->getCurrentRequest();
    if ($route = $request->attributes->get(\Drupal\Core\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
      $route->setDefault('_title', 'Learning Path Form');
    }

    // Get certificate list.
    $query = $this->database->select('opigno_certificate', 'oc');
    $query->fields('oc', ['id', 'bundle', 'uuid', 'label', 'uid']);
    $certificate_results = $query->execute()->fetchAll();

    // Get m list.
    $query = $this->database->select('opigno_module_field_data', 'om');
    $query->fields('om', ['id', 'name']);
    $module_results = $query->execute()->fetchAll();

    // Get acitivity list.
    $query = $this->database->select('opigno_activity_field_data', 'oa');
    $query->fields('oa', ['id', 'name']);
    $activity_results = $query->execute()->fetchAll();

    // Create options for certificate.
    $create_cer_options = ['_none' => '- None -'];
    if (!empty($certificate_results)) {
      foreach ($certificate_results as $value) {
        if (!empty($value)) {

          $create_cer_options[$value->bundle] = [$value->id => $value->label];
        }
      }
    }

    // Create options for module.
    $create_m_options = ['_none' => '- None -'];
    if (!empty($module_results)) {
      foreach ($module_results as $mvalue) {
        if (!empty($mvalue)) {

          $create_m_options[$mvalue->id] = $mvalue->name;
        }
      }
    }

    // Create options for activity.
    $create_a_options = ['_none' => '- None -'];
    if (!empty($activity_results)) {
      foreach ($activity_results as $avalue) {
        if (!empty($avalue)) {

          $create_a_options[$avalue->id] = $avalue->name;
        }
      }
    }

    $form['#prefix'] = '<div id="learning-path-div">';
    $form['#suffix'] = '</div>';

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
      '#default_value' => '',
      '#placeholder' => $this->t('Enter Title'),
      '#attributes' => [
        'class' => [
          'lp-title-field',
        ],
      ],
    ];

    $form['certificate'] = [
      '#type' => 'select',
      '#title' => $this->t('Certificate'),
      '#required' => TRUE,
      '#default_value' => $config->get('certificate'),
      '#options' => $create_cer_options,
      '#attributes' => [
        'class' => [
          'lp-certificate-field',
        ],
      ],

    ];

    $form['module_list_div']['#prefix'] = '<div id="module-list-div">';
    $form['module_list_div']['#suffix'] = '</div>';

    $form['module_list_div']['module_list'] = [
      '#type' => 'select',
      '#title' => $this->t('Select module'),
      '#default_value' => '',
      '#options' => $create_m_options,
      '#attributes' => [
        'class' => [
          'module-list-field',
        ],
      ],
      '#id' => 'module-list-field',

    ];

    $form['enable_module_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create New Module'),
      '#attributes' => [
        'class' => [
          'checkbox-for-module-fields',
        ],
      ],
      '#id' => 'checkbox-for-module-fields',

    ];

    $form['module_fields']['#prefix'] = '<div id="module-fields-div">';
    $form['module_fields']['#suffix'] = '</div>';

    $form['module_fields']['module_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Module Name'),
      '#default_value' => '',
      '#attributes' => [
        'class' => [
          'module-name-field',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="enable_module_fields"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['module_fields']['module_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Module Description'),
      '#default_value' => '',
      '#attributes' => [
        'class' => [
          'module-description-field',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="enable_module_fields"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => '<div class="act_ar">',
    ];

    $form['activities_list'] = [
      '#type' => 'select',
      '#title' => $this->t('Select activity'),
      '#prefix' => '<div id="replace_activities_list">',
      '#suffix' => '</div>',
      '#default_value' => '',
      '#options' => $create_a_options,
      '#attributes' => [
        'class' => [
          'lp-activities-list',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="enable_module_fields"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['refresh_button'] = [
      '#type' => 'button',
      '#value' => $this->t('Refresh'),
      '#submit' => ['secondary_submit_function'],
      '#prefix' => '<div class="button-refresh-in">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => [$this, 'dynamicActivityForm'],
        'event' => 'click',
        'method' => 'replace',
        'effect' => 'fade',
      ],
      '#states' => [
        'visible' => [
          ':input[name="enable_module_fields"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['helpclose'] = [
      '#type' => 'markup',
      '#markup' => '</div>',
    ];

    $form['module_activity']['#prefix'] = '<div id="module-fields-div-activity">';
    $form['module_activity']['#suffix'] = '</div>';

    $form['module_activity']['create_new_activity'] = [
      '#type' => 'link',
      '#title' => $this->t('Add New Activity'),
      '#url' => Url::fromUri('internal:/admin/structure/opigno_activity/add/opigno_h5p'),
      '#attributes' => [
        'class' => [
          'create-new-activity-link',
        ],
        'target' => '_blank',
      ],
      '#weight' => 3,
    ];

    $form['enable_advanced'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable advanced fields'),
    ];

    $form['takes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of attempts'),
      '#default_value' => $config->get('takes'),
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['minimum_score_to_validate_step'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum score'),
      '#default_value' => $config->get('minimum_score'),
      '#attributes' => [
        'class' => [
          'lp-minimum-score-field',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['pass_percentage'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'type' => 'number',
      ],
      '#title' => $this->t('Pass Percentage score'),
      '#default_value' => $config->get('pass_percentage'),
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['hide_results'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Review correct answers'),
      '#default_value' => $config->get('hide_results'),
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['hide_check_answers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide check answers'),
      '#default_value' => $config->get('hide_check_answers'),
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['hide_result_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide result page'),
      '#default_value' => $config->get('hide_result_page'),
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['hide_solution_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide solution button'),
      '#default_value' => $config->get('hide_solution_button'),
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['hide_retry_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide retry button'),
      '#default_value' => $config->get('hide_retry_button'),
      '#states' => [
        'visible' => [
          ':input[name="enable_advanced"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'opigno_lp_create/opigno_lp_create';

    return $form;
  }

  /**
   * Create activity.
   */
  public function dynamicActivityForm(array &$form, FormStateInterface &$form_state) : AjaxResponse {

    // Get acitivity list.
    $query = $this->database->select('opigno_activity_field_data', 'oa');
    $query->fields('oa', ['id', 'name']);
    $activity_results = $query->execute()->fetchAll();
    // Create a new textfield element containing the selected text.
    // Create options for activity.
    $create_a_options = ['_none' => '- None -'];
    if (!empty($activity_results)) {
      foreach ($activity_results as $avalue) {
        if (!empty($avalue)) {
          $create_a_options[$avalue->id] = $avalue->name;
        }
      }
    }

    $form['activities_list'] = [
      '#type' => 'select',
      '#title' => $this->t('Select activity'),
      '#default_value' => '',
      '#options' => $create_a_options,
      '#attributes' => [
        'name' => ['activities_list'],
        'id' => ['edit-output'],
        'class' => [
          'lp-activities-list',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="enable_module_fields"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#replace_activities_list', $this->renderer->renderRoot($form['activities_list'])));
    $form_state->setRebuild(TRUE);
    return $response;
  }

  /**
   * Validate datas for training form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('op') == 'Refresh') {
      // Set a validation error message.
      $form_state->setErrorByName('op', $this->t('The operation cannot be "Refresh".'));
    }

    if (empty($form_state->getValue('title'))) {
      $form_state->setErrorByName('title', $this->t('Title field is required.'));
    }
    if ($form_state->getValue('certificate') === '_none') {
      $form_state->setErrorByName('certificate', $this->t('The certificate field is required.'));
    }
    if (!preg_match('/^[0-9]*$/', $form_state->getValue('minimum_score_to_validate_step'))) {
      $form_state->setErrorByName('minimum_score_to_validate_step', $this->t('The score must be an integer.'));
    }

    if (!empty($form_state->getValue('minimum_score_to_validate_step')) && $form_state->getValue('minimum_score_to_validate_step') > 100) {
      $form_state->setErrorByName('minimum_score_to_validate_step', $this->t('The score must be an integer between 0 to 100.'));
    }
    if ($form_state->getValue('enable_module_fields') == 0 && $form_state->getValue('module_list') === '_none') {
      $form_state->setErrorByName('module_list', $this->t('Please select module.'));
    }
    if ($form_state->getValue('enable_module_fields') == 1) {

      if (empty($form_state->getValue('module_name'))) {
        $form_state->setErrorByName('module_name', $this->t('The module name field is required.'));
      }
      elseif (empty($form_state->getValue('module_description'))) {
        $form_state->setErrorByName('module_description', $this->t('The description field is required.'));
      }
    }

    if ($form_state->getValue('activities_list') == '_none' && $form_state->getValue('module_list') == '_none') {
      $form_state->setErrorByName('activities_list', $this->t('The activity field is required.'));
    }
  }

  /**
   * Save datas for training.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $lp_id = $this->opignoLpCreate->lpCreate($form_state);

    if ($lp_id) {
      $this->messenger->addMessage($this->t('Learning Path is unmapped Successfully'), 'status', TRUE);
    }
    else {
      $this->messenger->addError($this->t('Something went wrong.'), 'status', TRUE);
    }
  }

}
