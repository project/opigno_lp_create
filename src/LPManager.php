<?php

namespace Drupal\opigno_lp_create;

use Drupal\Core\Entity\EntityManager;

/**
 * This is a Drupal 9 custom service.
 */
class LPManager {

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */

  private $entityManager;

  /**
   * Initial constructor.
   */
  public function __construct(EntityManager $entityManager) {
    $this->entityManager = $entityManager;
  }

}
