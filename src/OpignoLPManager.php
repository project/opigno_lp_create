<?php

namespace Drupal\opigno_lp_create;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\h5p\Entity\H5PContent;
use Drupal\opigno_module\Controller\OpignoModuleController;
use Drupal\opigno_module\Entity\OpignoActivity;
use Drupal\opigno_module\Entity\OpignoModule;

/**
 * This is a Drupal 9 custom learning path service.
 */
class OpignoLPManager {

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityManager;
  /**
   * {@inheritdoc}
   */
  private $configFactory;
  /**
   * {@inheritdoc}
   */
  private $database;

  /**
   * {@inheritdoc}
   */
  public $opignoModuleObj;

  /**
   * Construct objects.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityManager
   *   Entity manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database service.
   * @param \Drupal\opigno_module\Controller\OpignoModuleController $opignoModuleObj
   *   The OpignoModuleController service.
   */
  public function __construct(EntityTypeManager $entityManager, ConfigFactoryInterface $configFactory, Connection $connection, OpignoModuleController $opignoModuleObj) {
    $this->entityManager = $entityManager;
    $this->configFactory = $configFactory;
    $this->database = $connection;
    $this->opignoModuleObj = $opignoModuleObj;
  }

  /**
   * Create a lesson plan.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object representing the current state of the form.
   */
  public function lpCreate($form_state) {

    $entity_type_manager = $this->entityManager;

    if (!empty($form_state->getValues())) {
      $label = !empty($form_state->getValue('title')) ? $form_state->getValue('title') : '';
      $config = $this->configFactory->getEditable('opigno_lp_create.lms_config');
      $minimum_score_to_validate_step = !empty($form_state->getValue('minimum_score_to_validate_step')) ? $form_state->getValue('minimum_score_to_validate_step') : $config->get('minimum_score');
      $certificate = !empty($form_state->getValue('certificate')) ? $form_state->getValue('certificate') : $config->get('certificate');
      $module_list = !empty($form_state->getValue('module_list')) ? $form_state->getValue('module_list') : '';
      $enable_module_fields = !empty($form_state->getValue('enable_module_fields')) ? $form_state->getValue('enable_module_fields') : 0;
      $module_name = !empty($form_state->getValue('module_name')) ? $form_state->getValue('module_name') : '';
      $module_description = !empty($form_state->getValue('module_description')) ? $form_state->getValue('module_description') : '';
      $activities_list = !empty($form_state->getValue('activities_list')) ? $form_state->getValue('activities_list') : '';
      $hide_results = $form_state->getValue('hide_results');
      $hide_check_answers = $form_state->getValue('hide_check_answers');
      $hide_result_page = $form_state->getValue('hide_result_page');
      $hide_solution_button = $form_state->getValue('hide_solution_button');
      $hide_retry_button = $form_state->getValue('hide_retry_button');
      $activities_config = [
        'hide_check_answers' => $hide_check_answers,
        'hide_result_page' => $hide_result_page,
        'hide_solution_button' => $hide_solution_button,
        'hide_retry_button' => $hide_retry_button,
      ];

      $takes = !empty($form_state->getValue('takes')) ? $form_state->getValue('takes') : $config->get('takes');
      $pass_percentage = !empty($form_state->getValue('pass_percentage')) ? $form_state->getValue('pass_percentage') : $config->get('pass_percentage');

    }
    if (!empty($module_list) && $enable_module_fields == 0 && $module_list != '_none') {
      // Get m list.
      $module_entity = OpignoModule::load($module_list);
      $this->updateScoreModule($module_entity, $minimum_score_to_validate_step, $pass_percentage, $activities_config);

    }
    elseif (!empty($module_name) && $enable_module_fields == 1 && $module_list === '_none') {
      // Create a Module/Course:
      $module_storage = $entity_type_manager->getStorage('opigno_module');
      if (!empty($module_description)) {
        $module_content = [
          'name' => $module_name,
          'description' => [
            'value' => $module_description,
            'format' => 'basic_html',
          ],
          'takes' => $takes,
          'hide_results' => $hide_results,
        ];
        $module_entity = $module_storage->create($module_content);
        $module_entity->save();
      }
      else {
        $module_content = [
          'name' => $module_name,
          'takes' => $takes,
          'hide_results' => $hide_results,
        ];
        $module_entity = $module_storage->create($module_content);
        $module_entity->save();
      }

      // Make a relations between activity into a module.
      if (!empty($activities_list)) {
        $activity_entity = OpignoActivity::load($activities_list);
        if (!empty($activity_entity)) {
          $this->opignoModuleObj->activitiesToModule([$activity_entity], $module_entity);
        }

        $this->activityConfig($activities_list, $pass_percentage, $activities_config);
      }
      $this->updateScoreModule($module_entity, $minimum_score_to_validate_step, $pass_percentage, $activities_config);

    }

    // Create an LP entity:
    $group_storage = $entity_type_manager->getStorage('group');
    $learning_path_content = [
      'type' => 'learning_path',
      'label' => $label,
      'rh_action' => 'bundle_default',
      'field_learning_path_published' => 1,
      'field_learning_path_description' => [
        'value' => 'static description...',
        'format' => 'basic_html',
      ],
      'field_certificate' => ['target_id' => $certificate],
    ];
    $learning_path_entity = $group_storage->create($learning_path_content);
    $learning_path_entity->save();

    // Create an opigno_group_content and Relationship.
    $group_content_storage = $entity_type_manager->getStorage('opigno_group_content');
    $group_content_entity = [
      'group_id' => $learning_path_entity->id(),
      'group_content_type_id' => 'ContentTypeModule',
      'entity_id' => $module_entity->id(),
      'success_score_min' => $minimum_score_to_validate_step,
      'is_mandatory' => 1,
      'coordinate_x' => 1,
      'coordinate_y' => 1,
      'in_skills_system' => 0,
    ];

    $group_content_storage->create($group_content_entity)->save();
    $learning_path_entity->addContent($module_entity, 'opigno_module_group');

    return $learning_path_entity->id();
  }

  /**
   * This method is called on learning path load.
   *
   * It will update an existing activity relation.
   */
  public function updateActivity($omr_ids, $max_score) {
    // First, check the params.
    foreach ($omr_ids as $omr_id) {
      $this->database->merge('opigno_module_relationship')
        ->keys(
                [
                  'omr_id' => $omr_id,
                ]
            )
        ->fields(
                [
                  'max_score' => $max_score,
                ]
            )
        ->execute();
    }
    return TRUE;
  }

  /**
   * It will update the score.
   */
  public function updateScoreModule($module_entity, $minimum_score_to_validate_step, $pass_percentage, $activities_config) {
    $query = $this->database->select('opigno_activity', 'oa');
    $query->fields('oafd', ['id', 'vid', 'type', 'name']);
    $query->fields(
          'omr', [
            'weight',
            'max_score',
            'auto_update_max_score',
            'omr_id',
            'omr_pid',
            'child_id',
            'child_vid',
          ]
      );
    $query->addJoin('inner', 'opigno_activity_field_data', 'oafd', 'oa.id = oafd.id');
    $query->addJoin('inner', 'opigno_module_relationship', 'omr', 'oa.id = omr.child_id');
    $query->condition('oafd.status', 1);
    $query->condition('omr.parent_id', $module_entity->id());
    if ($module_entity->getRevisionId()) {
      $query->condition('omr.parent_vid', $module_entity->getRevisionId());
    }
    $query->condition('omr_pid', NULL, 'IS');
    $query->orderBy('omr.weight');
    $result = $query->execute()->fetchAll();
    $omr_ids = [];
    foreach ($result as $activity) {
      $omr_ids[] = $activity->omr_id;
      $this->activityConfig($activity->id, $pass_percentage, $activities_config);
    }
    return $this->updateActivity($omr_ids, $minimum_score_to_validate_step);

  }

  /**
   * It will hide the no' of fields for the h5p activity.
   */
  public function activityConfig($activities_list, $activities_config, $pass_percentage = NULL) {
    $opignoActivity = OpignoActivity::load($activities_list);
    if (!empty($opignoActivity->opigno_h5p) && $opignoActivity->opigno_h5p != NULL) {
      foreach ($opignoActivity->opigno_h5p as $h5p) {
        $h5p_content = H5PContent::load($h5p->h5p_content_id);
        if ($h5p_content->parameters->value || $h5p_content->filtered_parameters->value) {
          $decode_hp5 = json_decode($h5p_content->filtered_parameters->value, TRUE);
          $decode_hp5['override']['checkButton'] = !empty($activities_config['hide_check_answers']) ? FALSE : TRUE;
          $decode_hp5['endGame']['showResultPage'] = !empty($activities_config['hide_result_page']) ? FALSE : TRUE;
          $decode_hp5['endGame']['showSolutionButton'] = !empty($activities_config['hide_solution_button']) ? FALSE : TRUE;
          $decode_hp5['endGame']['showRetryButton'] = !empty($activities_config['hide_retry_button']) ? FALSE : TRUE;
          $decode_hp5['behaviour']['enableRetry'] = !empty($activities_config['hide_retry_button']) ? FALSE : TRUE;
          $decode_hp5['behaviour']['enableSolutionsButton'] = !empty($activities_config['hide_solution_button']) ? FALSE : TRUE;
          $decode_hp5['behaviour']['enableCheckButton'] = !empty($activities_config['hide_check_answers']) ? FALSE : TRUE;

          if (isset($decode_hp5['questions']) && $decode_hp5['questions'] != NULL) {
            foreach ($decode_hp5['questions'] as $key => $row) {
              $decode_hp5['questions'][$key]['params']['behaviour']['enableRetry'] = !empty($activities_config['hide_retry_button']) ? FALSE : TRUE;
              $decode_hp5['questions'][$key]['params']['behaviour']['enableSolutionsButton'] = !empty($activities_config['hide_solution_button']) ? FALSE : TRUE;
              $decode_hp5['questions'][$key]['params']['behaviour']['enableCheckButton'] = !empty($activities_config['hide_check_answers']) ? FALSE : TRUE;

            }
          }
          if ($pass_percentage) {
            $decode_hp5['passPercentage'] = $pass_percentage;
          }
          $h5p_content->filtered_parameters->value = json_encode($decode_hp5);
          $h5p_content->save();
        }
      }
    }

  }

}
